FROM ubuntu AS builder
LABEL maintainer="Michael Englert <michi.eng@gmail.com>"

ARG BASEURL="https://download.appdynamics.com/download/prox/download-file"
ARG VERSION
ARG USER
ARG PASSWORD

ADD eum.response /tmp/
ADD start.sh /opt/appdynamics/eum/

RUN apt-get update
RUN apt-get install --fix-missing -q -y libaio1 libnuma1 net-tools curl tar
RUN echo "ulimit -n 65535" >> /etc/profile
RUN echo "ulimit -u 8192" >> /etc/profile
RUN echo "vm.swappiness = 10" >> /etc/sysctl.conf
RUN curl --referer http://www.appdynamics.com -c /tmp/cookies.txt -d "username=${USER}&password=${PASSWORD}" https://login.appdynamics.com/sso/login/
RUN curl -L -o /tmp/euem-64bit-linux.sh -b /tmp/cookies.txt ${BASEURL}/euem-processor/${VERSION}/euem-64bit-linux-${VERSION}.sh
RUN chmod +x /tmp/euem-64bit-linux.sh
RUN chmod +x /opt/appdynamics/eum/start.sh
RUN /tmp/euem-64bit-linux.sh -q -varfile /tmp/eum.response

FROM ubuntu
LABEL maintainer="Michael Englert <michi.eng@gmail.com>"

COPY --from=builder /opt/appdynamics /opt/appdynamics
COPY --from=builder /etc/profile /etc/profile
COPY --from=builder /etc/sysctl.conf /etc/sysctl.conf

ENV APPDYNAMICS_EUM_ANALYTICS_SCHEME="http" \
    APPDYNAMICS_EUM_ANALYTICS_HOST="localhost" \
    APPDYNAMICS_EUM_ANALYTICS_PORT="9080" \
    APPDYNAMICS_EUM_ANALYTICS_KEY=""

RUN apt-get update \
    && apt-get install --fix-missing -q -y libaio1 libnuma1

EXPOSE 7001

CMD [ "/bin/sh", "-c", "/opt/appdynamics/eum/start.sh" ]